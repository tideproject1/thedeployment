resource "aws_ecs_cluster" "flask_app" {
  name = "flask-app"
}

resource "aws_ecs_task_definition" "flask_app" {
  family                = "flask_app"
  network_mode          = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                    = "256"
  memory                 = "512"
  execution_role_arn     = aws_iam_role.flask_app.arn

  container_definitions = jsonencode([
    {
      "name": "flask_app",
      "image": "dragonbjekic/simpleflaskapp:${var.APP_VERSION}",
      "essential": true,
      "repositoryCredentials": {
            "credentialsParameter": "${aws_secretsmanager_secret.registry_credentials.arn}"
        }
      "portMappings": [
        {
          "containerPort": 5000,
          "hostPort": 5000
        }
      ]
    }
  ])
}

resource "aws_iam_role" "flask_app" {
  name = "flask_app"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_ecs_service" "flask_app" {
  name            = "flask-app"
  cluster         = aws_ecs_cluster.flask_app.id
  desired_count     = 1
  task_definition = aws_ecs_task_definition.flask_app.arn
  launch_type     = "FARGATE"

  network_configuration {
    subnets = [aws_subnet.public.id]
    security_groups = [aws_security_group.sg.id]
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.target_group.arn
    container_name   = "flask_app"
    container_port   = 5000
  }
}
