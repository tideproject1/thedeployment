terraform {
backend "s3" {
    region = "eu-west-1"
    bucket = "dragonbjekic-terraformstates"
    key    = "tideApp.tfstate"
}

  required_providers {

    aws = {
      version = "~> 5.48"
    }
  }
}
