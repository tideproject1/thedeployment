resource "aws_secretsmanager_secret" "registry_credentials" {
  name = "registry_credentials"
}

data "aws_iam_policy_document" "registry_credentials" {
  statement {
    actions = [
      "secretsmanager:GetSecretValue",
    ]

    resources = [
      aws_secretsmanager_secret.registry_credentials.arn
    ]
  }
}

resource "aws_iam_role_policy" "flask_app_registry_credentials" {
  name   = "flask-app-registry-credentials"
  role   = aws_iam_role.flask_app.id
  policy = data.aws_iam_policy_document.registry_credentials.json
}
