data "aws_vpc" "default" {
  default = true
}

data "aws_internet_gateway" "igw" {
  filter {
    name   = "attachment.vpc-id"
    values = [data.aws_vpc.default.id]
  }
}

resource "aws_subnet" "public" {
  vpc_id     = data.aws_vpc.default.id
  cidr_block = "172.31.100.0/24"
}


resource "aws_route_table" "public" {
  vpc_id = data.aws_vpc.default.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = data.aws_internet_gateway.igw.id
  }
}

resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.public.id
  route_table_id = aws_route_table.public.id
}

resource "aws_security_group" "sg" {
  name        = "flask-app"
  description = "Allow all inbound traffic to flask apps port 5000"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}
resource "aws_lb" "lb" {
  name               = "flask-app-fargate-lb"
  internal           = false
  load_balancer_type = "network"
  subnet_mapping {
    subnet_id = aws_subnet.public.id
    allocation_id = aws_eip.lb.allocation_id
  }
}

resource "aws_lb_target_group" "target_group" {
  name     = "flask-app-fargate-tg"
  port     = 5000
  protocol = "TCP"
  vpc_id   = data.aws_vpc.default.id
  target_type = "ip"
}

resource "aws_lb_listener" "listener" {
  load_balancer_arn = aws_lb.lb.arn
  port              = 5000
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target_group.arn
  }
}

resource "aws_eip" "lb" {
  domain = "vpc"
}

output "flask-app-lb-eip" {
    value = aws_eip.lb.public_ip
}
